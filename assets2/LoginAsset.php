<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets2;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/styles.css"
    ];
    public $js = [
        // 'plugins/jquery/jquery.min.js',
        // 'plugins/bootstrap/js/bootstrap.bundle.min.js',
        // 'dist/js/adminlte.min.js'
        "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js",
        "cdn-cgi/challenge-platform/h/g/scripts/invisible.js?ts=1655380800",
        "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js",
        "js/scripts.js",
        "https://assets.startbootstrap.com/js/sb-customizer.js"
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
