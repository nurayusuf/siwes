<?php

namespace app\components;

use Yii;

class Flash
{
    // ALERTS

    public static function alertSuccess(string $message): void
    {
        Yii::$app->session->setFlash('alert:success', $message);
    }

    public static function alertInfo(string $message): void
    {
        Yii::$app->session->setFlash('alert:info', $message);
    }

    public static function alertWarning(string $message): void
    {
        Yii::$app->session->setFlash('alert:warning', $message);
    }

    public static function alertError(string $message): void
    {
        Yii::$app->session->setFlash('alert:danger', $message);
    }

    public static function alertDanger(string $message): void
    {
        Yii::$app->session->setFlash('alert:danger', $message);
    }

    // TOASTS

    public static function toastSuccess(string $message): void
    {
        Yii::$app->session->setFlash('toast:success', $message);
    }

    public static function toastInfo(string $message): void
    {
        Yii::$app->session->setFlash('toast:info', $message);
    }

    public static function toastWarning(string $message): void
    {
        Yii::$app->session->setFlash('toast:warning', $message);
    }

    public static function toastError(string $message): void
    {
        Yii::$app->session->setFlash('toast:error', $message);
    }

    public static function toastDanger(string $message): void
    {
        Yii::$app->session->setFlash('toast:error', $message);
    }


    public static function renderAllFlashMessages(): ?string
    {
        return
            // ALERTS
            self::renderFlashAlertMessage('alert:danger', 'danger')
            . self::renderFlashAlertMessage('alert:error', 'error')
            . self::renderFlashAlertMessage('alert:warning')
            . self::renderFlashAlertMessage('alert:info', 'info')
            . self::renderFlashAlertMessage('alert:success', 'success')
            // TOASTS
            . self::renderFlashToastMessage('toast:danger', 'danger')
            . self::renderFlashToastMessage('toast:error', 'error')
            . self::renderFlashToastMessage('toast:warning')
            . self::renderFlashToastMessage('toast:info', 'info')
            . self::renderFlashToastMessage('toast:success', 'success');
    }

    protected static function renderFlashAlertMessage(string $key, string $type = 'warning'): ?string
    {
        $message = Yii::$app->session->getFlash($key);
        if (!$message) return null;
        $faIcon = self::getFAIcon($type);

        return "
            <div class=\"mx-5 z-depth-1 alert alert-$type\">
                <i class=\"fa fa-$faIcon\"></i> 
                $message
                <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
            </div>
        ";
    }

    protected static function renderFlashToastMessage(string $key, string $type = 'warning'): ?string
    {
        $message = Yii::$app->session->getFlash($key);
        if (!$message) return null;

        return sprintf('<script>window.addEventListener("load", () => toastr.%s("%s"));</script>', $type, $message);
    }

    protected static function getFAIcon(string $type): string
    {
        switch ($type) {
            case 'success':
                return 'check-double';
            case 'danger':
                return 'skull-crossbones';
            case 'warning':
                return 'exclamation-circle';
            default:
                return 'info-circle';
        }
    }
}