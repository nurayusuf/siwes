<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Supervisors $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="Supervisors-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="mb-3">
        <label class="small mb-1" for="inputUsername">Fullname (how it appears on your certifcates)</label>
        <?= $form->field($model, 'full_name')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
    </div>

    <div class="row gx-3">
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputLastName">Email</label>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false)  ?>
        </div>
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputFirstName">Mobile Number</label>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false)  ?>
        </div>
    </div>

    <div class="row gx-3">
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputFirstName">Department</label>
            <?= $form->field($model, 'department')->dropDownList(
                ArrayHelper::merge(['' => 'Select A Student Departmrnt'], ['Computer Science' => 'Computer Science', 'Accounting' => 'Accounting']),
                ['class' => 'form-control custom-select'],
            )->label(false) ?>
        </div>
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputLastName">Address</label>
            <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false)  ?>
        </div>
    </div>

    <hr class="my-4" />
    <div class="d-flex justify-content-end">
        <?php echo Html::submitButton('Save', ['class' => 'btn btn-primary', 'value' => 'save', 'name' => 'submitbtn']); ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>