<script type="text/javascript">
    <?php
    if (Yii::$app->session->hasFlash('error')) : ?>

        Swal.fire({
            position: 'center',
            icon: 'error',
            title: "<?= Yii::$app->session->getFlash('error') ?>",
            showConfirmButton: false,
            customClass: 'swal-text',
            timer: 3500
        });
    <?php endif; ?>

    <?php
    if (Yii::$app->session->hasFlash('warning')) : ?>

        Swal.fire({
            position: 'center',
            icon: 'warning',
            title: "<?= Yii::$app->session->getFlash('warning') ?>",
            showConfirmButton: false,
            customClass: 'swal-text',
            timer: 3500
        });
    <?php endif; ?>

    <?php
    if (Yii::$app->session->hasFlash('success')) : ?>

        Swal.fire({
            position: 'center',
            icon: 'success',
            title: "<?= Yii::$app->session->getFlash('success') ?>",
            showConfirmButton: false,
            customClass: 'swal-text',
            timer: 3500
        });
    <?php endif; ?>
</script>