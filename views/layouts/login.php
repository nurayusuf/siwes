<?php

/**
 * @var yii\web\View $this
 * @var string $content 
 */

use app\components\Flash;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= $this->title ?></title>
    <link href="/web/css/styles.css" rel="stylesheet" />
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <?php $this->head() ?>
</head>

<body class="bg-primary">
    <?php $this->beginBody() ?>
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main class="bg-primary">
                <?= $content ?>
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <footer class="footer-admin mt-auto footer-dark">
                <div class="container-xl px-4">
                    <div class="row">
                        <div class="col-md-6 small"> &copy; <?= date('Y') ?> SIWES</div>
                        <div class="col-md-6 text-md-end small">
                            <a href="https://schoolmanager.info">Powered By: Schoolmanager Edusoft</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <?php $this->endBody() ?>
</body>



</html>
<?php $this->endPage() ?>
<?= $this->render('notification') ?>