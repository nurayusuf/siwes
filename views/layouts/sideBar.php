<?php

use yii\helpers\Html;
?>
<nav class="sidenav shadow-right sidenav-light">
    <div class="sidenav-menu">
        <div class="nav accordion" id="accordionSidenav">
            <!-- Sidenav Menu Heading (Account)-->

            <!-- Sidenav Heading (Addons)-->
            <div class="sidenav-menu-heading">Menu</div>
            <!-- Sidenav Link (Charts)-->
            <a class="nav-link" href="/users/dashboard">
                <div class="nav-link-icon"><i data-feather="bar-chart"></i></div>
                Dashboard
            </a>
            <?php if(Yii::$app->user->identity->role == "Coordinator") : ?>

            <a class="nav-link" href="/users">
                <div class="nav-link-icon"><i data-feather="filter"></i></div>
                Users
            </a>

             <a class="nav-link" href="/students">
                <div class="nav-link-icon"><i data-feather="filter"></i></div>
                Students
            </a>

            <a class="nav-link" href="/supervisors">
                <div class="nav-link-icon"><i data-feather="filter"></i></div>
                Supervisors
            </a>
            <?php endif; ?>

            <a class="nav-link" href="/supervisor-and-student-assignment">
                <div class="nav-link-icon"><i data-feather="filter"></i></div>
                Assigned Students
            </a>

        </div>
    </div>
    <!-- Sidenav Footer-->
    <div class="sidenav-footer">
        <div class="sidenav-footer-content">
            <div class="sidenav-footer-subtitle">Logged in as:</div>
            <div class="sidenav-footer-title"><?= Yii::$app->user->identity->username; ?></div>
        </div>
    </div>
</nav>