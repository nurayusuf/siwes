<footer class="footer-admin mt-auto footer-light">
    <div class="container-xl px-4">
        <div class="row">
            <div class="col-md-6 small">
                <strong>
                    &copy; <?= date('Y') ?> SIWES
                </strong>
            </div>
            <div class="col-md-6 text-md-end small">
                <a href="#">Powered by AYAKOKEE</a>
            </div>
        </div>
    </div>
</footer>