<?php

use app\models\Users;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\UsersSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <!-- Main page heading-->
    <?= $this->render('@app/views/layouts/heading.php', ['heading' => $this->title, 'description' => 'A list of Users']) ?>

    <!-- Main page content-->
    <div class="container-xl px-4 mt-n10">
        <div class="card mb-4">
            <div class="card-header">
                <span class="float-right">
                    <?= Html::a(Yii::t('app', 'Add A New User'), ['create'], ['class' => 'btn btn-primary']) ?>
                </span>
            </div>
            <div class="card-body">
                <div class="table-responsive">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id',
                            'username',
                            'email:email',
                            // 'password',
                            'role',
                            'status',
                            //'created_by',
                            //'created_at',
                            //'updated_at',
                            //'updated_by',
                            [
                                'class' => ActionColumn::className(),
                                'urlCreator' => function ($action, Users $model, $key, $index, $column) {
                                    return Url::toRoute([$action, 'id' => $model->id]);
                                }
                            ],
                        ],
                    ]); ?>


                </div>
            </div>
        </div>
    </div>
</div>