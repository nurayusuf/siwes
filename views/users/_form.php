<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Users $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row gx-3">
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputLastName">Email</label>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false)  ?>
        </div>
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputFirstName">Username</label>
            <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false)  ?>
        </div>
    </div>

    <div class="row gx-3">
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputLastName">Role</label>
            <?= $form->field($model, 'role')->dropDownList(
                ArrayHelper::merge(['' => 'Select A User Role'], Yii::$app->params['roles']),
                ['class' => 'form-control custom-select'],
            )->label(false) ?>        </div>
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputFirstName">Password</label>
            <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false)  ?>
        </div>
    </div>

    <hr class="my-4" />
    <div class="d-flex justify-content-end">
        <?php echo Html::submitButton('Save', ['class' => 'btn btn-primary', 'value' => 'save', 'name' => 'submitbtn']); ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>