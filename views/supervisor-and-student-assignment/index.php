<?php

use app\models\SupervisorAndStudentAssignment;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\SupervisorAndStudentAssignmentSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Supervisor And Student Assignments');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Main page heading-->
<?= $this->render('@app/views/layouts/heading.php', ['heading' => $this->title, 'description' => 'A list of all SupervisorAndStudentAssignment and students assignments']) ?>

<!-- Main page content-->
<div class="container-xl px-4 mt-n10">
    <div class="card mb-4">
        <div class="card-header">
        <?php if(Yii::$app->user->identity->role == "Coordinator") : ?>
            <span class="float-right">
                <?= Html::a(Yii::t('app', 'Add A New Assignment'), ['create'], ['class' => 'btn btn-primary']) ?>
            </span>
            <?php endif; ?>
        </div>
        <div class="card-body">
            <div class="table-responsive">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        'Supervisor.full_name',
                        'student.student_name',
                        'organization_name',
                        'organization_address',
                        //'address',
                        //'created_by',
                        //'created_at',
                        //'updated_at',
                        //'updated_by',
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, SupervisorAndStudentAssignment $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'id' => $model->id]);
                            }
                        ],
                    ],
                ]); ?>


            </div>
        </div>
    </div>
</div>