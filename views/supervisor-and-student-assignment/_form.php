<?php

use app\utils\DbUtils;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\SupervisorAndStudentAssignment $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="Supervisor-and-student-assignment-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="mb-3">
        <label class="small mb-1" for="inputUsername">Organization Name</label>
        <?= $form->field($model, 'organization_name')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
    </div>

    <div class="mb-3">
        <label class="small mb-1" for="inputUsername">Organization Address</label>
        <?= $form->field($model, 'organization_address')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
    </div>

    <div class="row gx-3">
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputFirstName">Student</label>
            <?= $form->field($model, 'student_id')->dropDownList(
                ArrayHelper::merge(['' => 'Select A Student Departmrnt'], DbUtils::GetStudents()),
                ['class' => 'form-control custom-select'],
            )->label(false) ?>
        </div>
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputLastName">Supervisor</label>
            <?= $form->field($model, 'supervisor_id')->dropDownList(
                ArrayHelper::merge(['' => 'Select A Student Departmrnt'], DbUtils::GetSupervisors()),
                ['class' => 'form-control custom-select'],
            )->label(false) ?>
        </div>
    </div>

    <hr class="my-4" />
    <div class="d-flex justify-content-end">
        <?php echo Html::submitButton('Save', ['class' => 'btn btn-primary', 'value' => 'save', 'name' => 'submitbtn']); ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>