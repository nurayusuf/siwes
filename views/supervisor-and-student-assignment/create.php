<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\SupervisorAndStudentAssignment $model */

$this->title = Yii::t('app', 'Create Supervisor And Student Assignment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Supervisor And Student Assignments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <!-- Wizard card example with navigation-->
    <div class="card">
        <div class="card-header border-bottom">
        </div>
        <div class="card-body">
            <div class="tab-content" id="cardTabContent">
                <!-- Wizard tab pane item 1-->
                <div class="tab-pane py-5 fade show active" id="wizard" role="tabpanel" aria-labelledby="wizard1-tab">
                    <div class="row justify-content-center">
                        <div class="col-xxl-10 col-xl-10">
                            <h3 class="text-primary mt-0"><?= Html::encode($this->title) ?></h3>
                            <h5 class="card-title mb-4">Supervisor & Student Assignment</h5>
                            <?= $this->render('_form', [
                                'model' => $model,
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
