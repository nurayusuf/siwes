<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Students $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="students-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="mb-3">
        <label class="small mb-1" for="inputUsername">Fullname (how it appears on your certifcates)</label>
        <?= $form->field($model, 'student_name')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false) ?>
    </div>

    <div class="row gx-3">
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputLastName">Matrice No</label>
            <?= $form->field($model, 'matrice_no')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false)  ?>
        </div>
        <div class="mb-3 col-md-6">
            <label class="small mb-1" for="inputFirstName">Mobile Number</label>
            <?= $form->field($model, 'phone_umber')->textInput(['maxlength' => true, 'class' => 'form-control'])->label(false)  ?>
        </div>
    </div>

    <div class="mb-3">
        <label class="small mb-1" for="inputUsername">Department</label>
        <?= $form->field($model, 'department')->dropDownList(
            ArrayHelper::merge(['' => 'Select A Student Departmrnt'], Yii::$app->params['departments']),
            ['class' => 'form-control custom-select'],
        )->label(false) ?>
    </div>

    <div class="row gx-3 mb-3">
        <div class="col-md-6 mb-0">
            <label class="small mb-1" for="inputBirthday">Course</label>
            <?= $form->field($model, 'course')->dropDownList(
                ArrayHelper::merge(['' => 'Select A Student Course'], Yii::$app->params['courses']),
                ['class' => 'form-control custom-select'],
            )->label(false) ?>
        </div>
        <div class="col-md-6 mb-md-0">
            <label class="small mb-1" for="inputPhone">Level</label>
            <?= $form->field($model, 'level')->dropDownList(
                ArrayHelper::merge(['' => 'Select Student Level'], Yii::$app->params['levels']),
                ['class' => 'form-control custom-select'],
            )->label(false) ?>
        </div>
    </div>

    <hr class="my-4" />
    <div class="d-flex justify-content-end">
        <?php echo Html::submitButton('Save', ['class' => 'btn btn-primary', 'value' => 'save', 'name' => 'submitbtn']); ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>