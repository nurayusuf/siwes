<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;


/**
 * @var yii\web\View $this
 * @var yii\bootstrap4\ActiveForm $form
 * @var app\models\LoginForm $model
 */

?>
<div class="bg-primary">
    <div class="container-xl px-4">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11">
                <!-- Social login form-->
                <div class="card my-5">
                    <div class="card-body px-5 text-center">
                        <img src="../../../../web/images/dumsac/logo-medium.png" width="100" class="mb-2 img-responsive" />
                        <div class="h3 fw-light"> Sign In</div>
                        <div class="small text-muted mb-2">Application Portal</div>
                    </div>
                    <hr class="my-0" />
                    <div class="card-body px-5">
                        <!-- Login form-->
                        <?php $form = ActiveForm::begin([]); ?>
                        <!-- Form Group (email address)-->
                        <div class="mb-3">
                            <label class="text-gray-600 small" for="emailExample">Email address</label>
                            <?= $form->field($model, 'username')->textInput(['class' => 'form-control form-control-solid', 'name' => 'username', 'id' => 'username', 'aria-label' => "Email Address", 'aria-describedby' => "emailExample"])->label(false) ?>
                            <!-- <input class="form-control form-control-solid" type="text" placeholder="" aria-label="Email Address" aria-describedby="emailExample" /> -->
                        </div>
                        <!-- Form Group (password)-->
                        <div class="mb-3">
                            <label class="text-gray-600 small" for="passwordExample">Password</label>
                            <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control form-control-solid', 'name' => 'password', 'id' => 'password', 'aria-label' => "Password", 'aria-describedby' => "passwordExample"])->label(false) ?>
                        </div>
                        <!-- Form Group (forgot password link)-->
                        <div class="mb-3"><a class="small" href="/application/site/recover-password">Forgot your password?</a></div>
                        <!-- Form Group (login box)-->
                        <div class="d-flex align-items-center justify-content-between mb-0">
                            <div class="form-check">
                                <input class="form-check-input" id="checkRememberPassword" type="checkbox" value="" />
                                <label class="form-check-label" for="checkRememberPassword">Remember password</label>
                            </div>
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary']) ?>
                            <!-- <a class="btn btn-primary" href="dashboard-1.html">Login</a> -->
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <hr class="my-0" />
                    <div class="card-body px-5 py-4">
                        <div class="small text-center">
                            New Applicant?
                            <a href="/application/site/register">Create an account!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>