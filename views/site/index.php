<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;


/**
 * @var yii\web\View $this
 * @var yii\bootstrap4\ActiveForm $form
 * @var app\models\LoginForm $model
 */

?>
<!-- <div class="bg-primary"> -->
<div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
        <main>
            <div class="container-xl px-4">
                <div class="row justify-content-center">
                    <!-- Create Organization-->
                    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11 mt-4">
                        <div class="card text-center h-100">
                            <div class="card-body px-5 pt-5 d-flex flex-column">
                                <div>
                                    <div class="h3 text-primary">Applicant</div>
                                    <p class="text-muted mb-4">Create or manage your application</p>
                                </div>
                                <div class="icons-org-create align-items-center mx-auto mt-auto">
                                    <i class="icon-users" data-feather="users"></i>
                                    <i class="icon-plus fas fa-plus"></i>
                                </div>
                            </div>
                            <div class="card-footer bg-transparent px-5 py-4">
                                <div class="small text-center"><a class="btn btn-block btn-primary" href="/application/site/login">New Applicant</a></div>
                            </div>
                        </div>
                    </div>
                    <!-- Join Organization-->
                    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11 mt-4">
                        <div class="card text-center h-100">
                            <div class="card-body px-5 pt-5 d-flex flex-column align-items-between">
                                <div>
                                    <div class="h3 text-secondary">Student</div>
                                    <p class="text-muted mb-4">Returning student</p>
                                </div>
                                <div class="icons-org-join align-items-center mx-auto">
                                    <i class="icon-user" data-feather="user"></i>
                                    <i class="icon-arrow fas fa-long-arrow-alt-right"></i>
                                    <i class="icon-users" data-feather="users"></i>
                                </div>
                            </div>
                            <div class="card-footer bg-transparent px-5 py-4">
                                <div class="small text-center"><a class="btn btn-block btn-secondary" href="/student/site/login">Returning Student</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <div id="layoutAuthentication_footer">
        <footer class="footer-admin mt-auto footer-dark">
            <div class="container-xl px-4">
                <div class="row">
                    <div class="col-md-6 small">
                        <strong>
                          &copy; <?= date('Y') ?> Sudawa College of Nursing and Midwifery (SCONAM)
                        </strong>
                    </div>
                    <div class="col-md-6 text-md-end small">
                        <a href="#">Powered by AYAKOKEE</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- </div> -->