<?php


namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        // $this->layout = '@app/views/layouts/main';
        $route        = explode("/", $this->route);

        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('error', "You must be signed in to continue");
            return $this->redirect('/site/login')->send();  // login path
        }

        // if (!Yii::$app->accessChecker->hasAccess($route[0]))
        //     throw new HttpException(403, "You dont have permission to access this content");

        return parent::beforeAction($action);
    }
}
