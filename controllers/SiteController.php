<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->redirect('/site/login');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/users/dashboard');
        }

        $model = new LoginForm();
        if (Yii::$app->request->post()) {
            $model->username = Yii::$app->request->post()['username'];
            $model->password = Yii::$app->request->post()['password'];

            if ($model->login()) {
                Yii::$app->session->setFlash('success', 'Successfully logged in!');
                return $this->redirect('/users/dashboard');
            }

            $model->password = '';
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return  $this->redirect('/site/login');
    }

    public function beforeAction($action)
    {
        $this->layout = '@app/views/layouts/login';
        return parent::beforeAction($action);
    }
}
