<?php

namespace app\models;

use Yii;
use app\utils\DbUtils;

/**
 * This is the model class for table "Supervisor_and_student_assignment".
 *
 * @property int $id
 * @property int $student_id
 * @property string|null $organization_name
 * @property string|null $organization_address
 * @property int|null $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property int|null $updated_by
 */
class SupervisorAndStudentAssignment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Supervisor_and_student_assignment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'supervisor_id'], 'required'],
            [['student_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['organization_name', 'organization_address'], 'string', 'max' => 255],
            [['student_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Student ID',
            'supervisor_id' => 'Supervisor ID',
            'organization_name' => 'Organization Name',
            'organization_address' => 'Organization Address',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function getStudent()
    {
        return $this->hasOne(Students::class, ['id' => 'student_id']);
    }

    public function getSupervisor()
    {
        return $this->hasOne(Supervisors::class, ['id' => 'supervisor_id']);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = DbUtils::getTimeStamp();
            $this->created_by = Yii::$app->user->identity->id ?? 1;
        }

        if (!$this->isNewRecord) {
            $this->updated_at = DbUtils::getTimeStamp();
            $this->updated_by = Yii::$app->user->identity->id ?? 1;
        }

        return parent::beforeSave($insert);
    }
}
