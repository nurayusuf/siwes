<?php

namespace app\models;

use app\models\Users as ModelsUsers;
use Yii;
use yii\web\IdentityInterface;
use app\utils\DbUtils;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property int|null $status
 * @property string|null $role
 * @property int|null $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property int|null $updated_by
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'required'],
            [['status', 'created_by', 'updated_by', 'supervisor_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'password', 'role'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'status' => 'Status',
            'role' => 'Role',
            'supervisor_id' => 'Supervisor',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return;
    }

    public static function findByUsername($username)
    {
        return static::find()->where(['username' => $username])->orWhere(['email' => $username])->one();
    }

    public function validateAuthKey($authKey)
    {
        //return $this->getAuthKey() === $authKey;
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        //return $this->auth_key;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = DbUtils::getTimeStamp();
            $this->created_by = Yii::$app->user->identity->id ?? 1;
            $this->setPassword($this->password);
            $this->status = true;
        }

        if (!$this->isNewRecord) {
            $this->updated_at = DbUtils::getTimeStamp();
            $this->updated_by = Yii::$app->user->identity->id ?? 1;
        }

        return parent::beforeSave($insert);
    }

    public static function addNewUser(Supervisors $supervisor) {
        $user = new Users();
        $user->username = $supervisor->email;
        $user->email = $supervisor->email;
        $user->role = "Supervisor";
        $user->password = "12345";
        $user->supervisor_id = $supervisor->id;
        $user->save(false);
    }

}
