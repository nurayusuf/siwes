<?php

namespace app\models;

use Yii;
use app\utils\DbUtils;

/**
 * This is the model class for table "students".
 *
 * @property int $id
 * @property string $student_name
 * @property string $matrice_no
 * @property string $phone_umber
 * @property string $level
 * @property string $course
 * @property string $department
 * @property int|null $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property int|null $updated_by
 */
class Students extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'students';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_name', 'matrice_no', 'phone_umber', 'level', 'course', 'department'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['student_name', 'phone_umber', 'level', 'course', 'department'], 'string', 'max' => 255],
            [['matrice_no'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_name' => 'Student Name',
            'matrice_no' => 'Matrice No',
            'phone_umber' => 'Phone Umber',
            'level' => 'Level',
            'course' => 'Course',
            'department' => 'Department',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = DbUtils::getTimeStamp();
            $this->created_by = Yii::$app->user->identity->id ?? 1;
        }

        if (!$this->isNewRecord) {
            $this->updated_at = DbUtils::getTimeStamp();
            $this->updated_by = Yii::$app->user->identity->id ?? 1;
        }

        return parent::beforeSave($insert);
    }
}
