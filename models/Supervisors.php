<?php

namespace app\models;

use Yii;
use app\utils\DbUtils;

/**
 * This is the model class for table "Supervisors".
 *
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string|null $department
 * @property int|null $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property int|null $updated_by
 */
class Supervisors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'supervisors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'email', 'phone', 'address'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['full_name', 'email', 'phone', 'address', 'department'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'department' => 'Department',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->created_at = DbUtils::getTimeStamp();
            $this->created_by = Yii::$app->user->identity->id ?? 1;
        }

        if (!$this->isNewRecord) {
            $this->updated_at = DbUtils::getTimeStamp();
            $this->updated_by = Yii::$app->user->identity->id ?? 1;
        }

        return parent::beforeSave($insert);
    }
}
