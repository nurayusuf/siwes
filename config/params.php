<?php
// use yii\helpers\Url;
// $baseUrl = Url::base(true);
return [
    'registrar' => 'ABDULRAZAK YAHAYA',
    'bsVersion' => '4.x',
    'imagePath' => "web/images/applicants/",
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
    'selectedSession' => 'academicSession',
    'levels' => [
        'Level 100' => 'Level 100',
        'Level 200' => 'Level 200',
        'Level 300' => 'Level 300',
        'Level 400' => 'Level 400',
    ],
    'courses' => [
        'Computer Science' => 'Computer Science',
        'Mathematics' => 'Mathematics',
        'Mechanical Engineering' => 'Mechanical Engineering',
        'Software Engineering' => 'Software Engineering',
    ],
    'departments' => [
        'Engineering' => 'Engineering',
        'Computer Science' => 'Computer Science',
        'Law' => 'Law',
        'Languages' => 'Languages',
    ],
    'roles' => [
        'Coordinator' => 'Coordinator',
        'Supervisor' => 'Supervisor',
    ],

];
