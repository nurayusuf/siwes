<?php

$host     = $_ENV['DATABASE_HOST'];
$database = $_ENV['DATABASE_NAME'];
$username = $_ENV['DATABASE_USER'];
$password = $_ENV['DATABASE_PASSWORD'];

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . $host . ';dbname=' . $database,
    'username' => $username,
    'password' => $password,
    'charset' => 'utf8',
];

