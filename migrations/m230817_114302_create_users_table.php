<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m230817_114302_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id'           => $this->primaryKey(),
            'username'     => $this->string(255)->notNull(),
            'email'        => $this->string(200)->notNull(),
            'password'     => $this->string(255)->notNull(),
            'status'       => $this->boolean(),
            'role'         => $this->string(),
            'supervisor_id'=> $this->integer()->null(),
            'created_by'   => $this->integer(),
            'created_at'   => $this->timestamp()->notNull(),
            'updated_at'   => $this->timestamp(),
            'updated_by'   => $this->integer(),
        ]);
        $this->batchInsert(
            'users',
            ['username', 'email', 'password',  'status', 'role', 'created_at'],
            [
                [
                    'admin',
                    'admin@gmail.com',
                    '$2y$13$yPqH9Nm.UhzQQ6/GnIkqIuNhaN8hFi1Sc2/SLGssTZpX8FQt./QIe',
                    true,
                    'Coordinator',
                    date('Y-m-d h:m:s')
                ]
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
