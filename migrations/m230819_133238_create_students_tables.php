<?php

use yii\db\Migration;

/**
 * Class m230819_133238_create_students_tables
 */
class m230819_133238_create_students_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%students}}', [
            'id'           => $this->primaryKey(),
            'student_name' => $this->string(255)->notNull(),
            'matrice_no'   => $this->string(200)->notNull(),
            'phone_umber'  => $this->string(255)->notNull(),
            'level'        => $this->string(255)->notNull(),
            'course'       => $this->string()->notNull(),
            'department'   => $this->string()->notNull(),
            'created_by'   => $this->integer(),
            'created_at'   => $this->timestamp()->notNull(),
            'updated_at'   => $this->timestamp(),
            'updated_by'   => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%students}}');
    }
}
