<?php

use yii\db\Migration;

/**
 * Class m230819_133218_create_supervisors_tables
 */
class m230819_133218_create_supervisors_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%supervisors}}', [
            'id'           => $this->primaryKey(),
            'full_name'    => $this->string(255)->notNull(),
            'email'        => $this->string(255)->notNull(),
            'phone'        => $this->string(255)->notNull(),
            'address'      => $this->string(255)->notNull(),
            'department'   => $this->string(),
            'created_by'   => $this->integer(),
            'created_at'   => $this->timestamp()->notNull(),
            'updated_at'   => $this->timestamp(),
            'updated_by'   => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%supervisors}}');
    }

}
