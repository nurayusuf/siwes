<?php

use yii\db\Migration;

/**
 * Class m230819_134437_create_supervisor_and_student_assignment_tables
 */
class m230819_134437_create_supervisor_and_student_assignment_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Supervisor_and_student_assignment}}', [
            'id'           => $this->primaryKey(),
            'supervisor_id' => $this->integer()->notNull(),
            'student_id' => $this->integer()->notNull(),
            'organization_name' => $this->string(),
            'organization_address' => $this->string(),
            'created_by'   => $this->integer(),
            'created_at'   => $this->timestamp()->notNull(),
            'updated_at'   => $this->timestamp(),
            'updated_by'   => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%Supervisor_and_student_assignment}}');
    }
}
