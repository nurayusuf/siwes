<?php

namespace app\utils\Html;

use Yii;

class HtmlUtils
{
    public const ALERT_SUCCESS = 'success';
    public const ALERT_INFO = 'info';
    public const ALERT_WARNING = 'warning';
    public const ALERT_ERROR = 'error';

    /**
     * Generate bootstrap html badge
     *
     * @param string $text badge text
     * @param string $color color of badge, example: badge-primary
     * @param string $type type of badge, example: badge-pill
     * @return string
     */
    public static function badge(string $text, string $color, string $type = 'pill'): string
    {
        return '<div class="badge z-depth-1-half badge-' . $type . ' badge-' . $color . '">' . $text . '</div>';
    }

    /**
     * Generate bootstrap html alert
     *
     * @param string $type
     * @param string $message
     * @param array $attributes
     * @return string
     */
    public static function alert(string $type, string $message, array $attributes = []): string
    {
        switch ($type) {
            case 'info':
                $faIcon = '<i class="fa fa-info-circle"></i>';
                break;
            case 'danger':
                $faIcon = '<i class="fa fa-skull-crossbones"></i>';
                break;
            case 'success':
                $faIcon = '<i class="fa fa-check-circle"></i>';
                break;
            case 'warning':
                $faIcon = '<i class="fa fa-exclamation-circle"></i>';
                break;
            default:
                $faIcon = '<i class="fa fa-info"></i>';
        }

        $attributes['class'] = "alert alert-$type z-depth-1";

        return '<div ' . self::constructHtmlAttributes($attributes) . ">$faIcon $message</div>";
    }

    protected static function constructHtmlAttributes(array $attributes): string
    {
        $htmlAttributes = '';
        array_walk($attributes, function (string $value, string $key) use (&$htmlAttributes) {
            $htmlAttributes .= " $key=\"$value\"";
        });

        return $htmlAttributes;
    }

    public static function nairaSymbol(bool $appendWhitespace = false): string
    {
        $appendWhitespace = $appendWhitespace ? ' ' : null;
        return '<span>&#x20A6;</span>' . $appendWhitespace;
    }

    public static function backLink(string $text, string $url, array $attributes = []): string
    {
        $attributes['href'] = $url;
        $attributes['class'] = ($attributes['class'] ?? null) . ' text-muted font-weight-bold';
        $htmlAttributes = self::constructHtmlAttributes($attributes);

        return "
            <a $htmlAttributes>
                <i class='fa fa-arrow-left'></i> $text
            </a>
        ";
    }

    /**
     * @param StepperItem ...$stepperItem
     * @return string
     */
    public static function stepper(StepperItem ...$stepperItem): string
    {
        $html = '';
        foreach ($stepperItem as $item){
            $html .= $item->generateHtml();
        }

        return "<div class=\"pt-4\">$html</div>";
    }

    public static function renderCsrfField(): string
    {
        return sprintf(
            '<input type="hidden" name="%s" value="%s" />',
            Yii::$app->request->csrfParam,
            Yii::$app->request->csrfToken
        );
    }

}