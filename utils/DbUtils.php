<?php

namespace app\utils;

use app\models\AcademicSession;
use app\models\Supervisors;
use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use app\models\Courses;
use app\models\CustomerCategories;
use app\models\Schools;
use app\models\Faculties;
use app\models\Hostels;
use app\models\Lgas;
use app\models\Programme;
use app\models\ProgrammeLevels;
use app\models\ProgrammeFees;
use app\models\ProgrammeSemesters;
use app\models\StudentRegistrationFees;
use app\models\Students;
use Faker\Core\Coordinates;

class DbUtils extends Component
{
    public const PENDING = 0;
    public const APPROVED = 1;
    public const CANCELLED = 2;


    public static function getTimeStamp()
    {
        return date('Y-m-d H:i:s');
    }

    public static function GetSupervisors(): array
    {
        return ArrayHelper::merge(["" => " Select A Supervisor"], ArrayHelper::map(Supervisors::find()->orderBy(['full_name' => SORT_ASC])->all(), 'id', 'full_name'));
    }

    public static function GetStudents(): array
    {
        return ArrayHelper::merge(["" => " Select A Student"], ArrayHelper::map(Students::find()->orderBy(['student_name' => SORT_ASC])->all(), 'id', 'student_name'));
    }

    public static function isActiveDate($dateFrom, $dateTo)
    {
        return (strtotime(date("Y-m")) >= strtotime($dateFrom) && strtotime(date("Y-m")) <= strtotime($dateTo)) ? true : false;
    }
}
